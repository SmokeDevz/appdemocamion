import 'package:aplicaciondeprueba/pages/ListPage.dart';
import 'package:aplicaciondeprueba/pages/MapsPage.dart';
import 'package:aplicaciondeprueba/pages/ProfilePage.dart';
import 'package:flutter/material.dart';

import 'package:convex_bottom_bar/convex_bottom_bar.dart';

class HomePage extends StatefulWidget {
  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  int _selectedIndex = 0;

  List<Widget> _widgetPantallas = [
    MapsPage(),
    ListPage(),
    ProfilePage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _widgetPantallas.elementAt(_selectedIndex),
        bottomNavigationBar: ConvexAppBar(
          items: [
            TabItem(icon: Icons.map_sharp, title: 'MAPA'),
            TabItem(icon: Icons.list, title: 'VIEAJES'),
            TabItem(icon: Icons.person, title: 'PERFIL')
          ],
          color: Colors.black,
          activeColor: Colors.orange,
          backgroundColor: Colors.white,
          initialActiveIndex: 0, //optional, default as 0
          onTap: clickBarBottom,
        ));
  }

  void clickBarBottom(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}
