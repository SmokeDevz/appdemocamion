import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapsPage extends StatefulWidget {
  @override
  MapsPageState createState() => MapsPageState();
}

class MapsPageState extends State<MapsPage> {
  Completer<GoogleMapController> _controller = Completer();
  List<Marker> _markers = <Marker>[];

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(-33.4577434, -70.65735782),
    zoom: 10.4746,
  );

  @override
  Widget build(BuildContext context) {


    _markers.add(
        Marker(
            markerId: MarkerId('SomeId'),
            position: LatLng(38.123,35.123),
            infoWindow: InfoWindow(
                title: 'The title of the marker'
            )
        )
    );

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Text('CAMIONES'),
        ),
        body: GoogleMap(initialCameraPosition: _kGooglePlex,markers:     Set<Marker>.of(_markers)
        ,),
        );
  }
}
