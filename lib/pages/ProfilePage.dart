import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  @override
  ProfilePageState createState() => ProfilePageState();
}

class ProfilePageState extends State<ProfilePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Text('PERFIL'),
        ),
        body: Center(
          child: Container(
            padding: EdgeInsets.fromLTRB(0, 32, 0, 0),
            child: Column(
              children: [
                Container(
                  child: Image.network("https://cofipe.com.br/wp-content/uploads/2017/03/avatar-site-cofipe-masculino-1.png"),
                )
              ,Container(
                  padding: EdgeInsets.fromLTRB(0, 25, 0, 0),
                  child: Text("Claudio Funes",style: TextStyle(fontSize: 22,fontWeight:FontWeight.bold),),
                ),
              Container(
                child: Text("kako@gmail.com"),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                child: Text("_Rol",style: TextStyle(fontWeight:FontWeight.bold ),),
              )],
            ),
          ),
        ));
  }
}
