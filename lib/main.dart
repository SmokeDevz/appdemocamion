import 'package:aplicaciondeprueba/pages/LoginPage.dart';
import 'package:aplicaciondeprueba/pages/MapsPage.dart';
import 'package:flutter/material.dart';

import 'dart:developer' as developer;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(

            inputDecorationTheme: InputDecorationTheme(

              hintStyle: TextStyle(color: Colors.grey),

            )),
        home: Scaffold(
          body: LoginPage(),
        ));
  }
}
