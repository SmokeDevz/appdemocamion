import 'package:flutter/material.dart';

class ListPage extends StatefulWidget {
  @override
  ListPageState createState() => ListPageState();
}

class ListPageState extends State<ListPage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Text('VIAJES'),
        ),
        body: ListView(
          children: [
            Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.arrow_drop_down_circle),
                    title: const Text('Encomienda'),
                    subtitle: Text(
                      'Tipo de carga Fragil',
                      style: TextStyle(color: Colors.black.withOpacity(0.6)),
                    ),
                  ),
                  Container(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        children: [
                          Text(
                            'Origen',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                          Text(
                            'Destino',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                          Text(
                            'Conductor',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                        ],
                      )),
                  ButtonBar(
                    alignment: MainAxisAlignment.start,
                    children: [
                      FlatButton(
                        textColor: const Color(0xFF6200EE),
                        onPressed: () {
                          // Perform some action
                        },
                        child: const Text('Iniciar'),
                      ),
                      FlatButton(
                        textColor: const Color(0xFF6200EE),
                        onPressed: () {
                          // Perform some action
                        },
                        child: const Text('Finalizar'),
                      ),
                    ],
                  ),
                  Image.asset('assets/card-sample-image.jpg'),
                  Image.asset('assets/card-sample-image-2.jpg'),
                ],
              ),
            ),
            Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.arrow_drop_down_circle),
                    title: const Text('Encomienda'),
                    subtitle: Text(
                      'Tipo de carga Fragil',
                      style: TextStyle(color: Colors.black.withOpacity(0.6)),
                    ),
                  ),
                  Container(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        children: [
                          Text(
                            'Origen',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                          Text(
                            'Destino',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                          Text(
                            'Conductor',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                        ],
                      )),
                  ButtonBar(
                    alignment: MainAxisAlignment.start,
                    children: [
                      FlatButton(
                        textColor: const Color(0xFF6200EE),
                        onPressed: () {
                          // Perform some action
                        },
                        child: const Text('Iniciar'),
                      ),
                      FlatButton(
                        textColor: const Color(0xFF6200EE),
                        onPressed: () {
                          // Perform some action
                        },
                        child: const Text('Finalizar'),
                      ),
                    ],
                  ),
                  Image.asset('assets/card-sample-image.jpg'),
                  Image.asset('assets/card-sample-image-2.jpg'),
                ],
              ),
            ),
            Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.arrow_drop_down_circle),
                    title: const Text('Encomienda'),
                    subtitle: Text(
                      'Tipo de carga Fragil',
                      style: TextStyle(color: Colors.black.withOpacity(0.6)),
                    ),
                  ),
                  Container(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        children: [
                          Text(
                            'Origen',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                          Text(
                            'Destino',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                          Text(
                            'Conductor',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                        ],
                      )),
                  ButtonBar(
                    alignment: MainAxisAlignment.start,
                    children: [
                      FlatButton(
                        textColor: const Color(0xFF6200EE),
                        onPressed: () {
                          // Perform some action
                        },
                        child: const Text('Iniciar'),
                      ),
                      FlatButton(
                        textColor: const Color(0xFF6200EE),
                        onPressed: () {
                          // Perform some action
                        },
                        child: const Text('Finalizar'),
                      ),
                    ],
                  ),
                  Image.asset('assets/card-sample-image.jpg'),
                  Image.asset('assets/card-sample-image-2.jpg'),
                ],
              ),
            ),
            Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.arrow_drop_down_circle),
                    title: const Text('Encomienda'),
                    subtitle: Text(
                      'Tipo de carga Fragil',
                      style: TextStyle(color: Colors.black.withOpacity(0.6)),
                    ),
                  ),
                  Container(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        children: [
                          Text(
                            'Origen',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                          Text(
                            'Destino',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                          Text(
                            'Conductor',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                        ],
                      )),
                  ButtonBar(
                    alignment: MainAxisAlignment.start,
                    children: [
                      FlatButton(
                        textColor: const Color(0xFF6200EE),
                        onPressed: () {
                          // Perform some action
                        },
                        child: const Text('Iniciar'),
                      ),
                      FlatButton(
                        textColor: const Color(0xFF6200EE),
                        onPressed: () {
                          // Perform some action
                        },
                        child: const Text('Finalizar'),
                      ),
                    ],
                  ),
                  Image.asset('assets/card-sample-image.jpg'),
                  Image.asset('assets/card-sample-image-2.jpg'),
                ],
              ),
            ),
            Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.arrow_drop_down_circle),
                    title: const Text('Encomienda'),
                    subtitle: Text(
                      'Tipo de carga Fragil',
                      style: TextStyle(color: Colors.black.withOpacity(0.6)),
                    ),
                  ),
                  Container(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        children: [
                          Text(
                            'Origen',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                          Text(
                            'Destino',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                          Text(
                            'Conductor',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                        ],
                      )),
                  ButtonBar(
                    alignment: MainAxisAlignment.start,
                    children: [
                      FlatButton(
                        textColor: const Color(0xFF6200EE),
                        onPressed: () {
                          // Perform some action
                        },
                        child: const Text('Iniciar'),
                      ),
                      FlatButton(
                        textColor: const Color(0xFF6200EE),
                        onPressed: () {
                          // Perform some action
                        },
                        child: const Text('Finalizar'),
                      ),
                    ],
                  ),
                  Image.asset('assets/card-sample-image.jpg'),
                  Image.asset('assets/card-sample-image-2.jpg'),
                ],
              ),
            ),
            Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.arrow_drop_down_circle),
                    title: const Text('Encomienda'),
                    subtitle: Text(
                      'Tipo de carga Fragil',
                      style: TextStyle(color: Colors.black.withOpacity(0.6)),
                    ),
                  ),
                  Container(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        children: [
                          Text(
                            'Origen',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                          Text(
                            'Destino',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                          Text(
                            'Conductor',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                        ],
                      )),
                  ButtonBar(
                    alignment: MainAxisAlignment.start,
                    children: [
                      FlatButton(
                        textColor: const Color(0xFF6200EE),
                        onPressed: () {
                          // Perform some action
                        },
                        child: const Text('Iniciar'),
                      ),
                      FlatButton(
                        textColor: const Color(0xFF6200EE),
                        onPressed: () {
                          // Perform some action
                        },
                        child: const Text('Finalizar'),
                      ),
                    ],
                  ),
                  Image.asset('assets/card-sample-image.jpg'),
                  Image.asset('assets/card-sample-image-2.jpg'),
                ],
              ),
            ),
            Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.arrow_drop_down_circle),
                    title: const Text('Encomienda'),
                    subtitle: Text(
                      'Tipo de carga Fragil',
                      style: TextStyle(color: Colors.black.withOpacity(0.6)),
                    ),
                  ),
                  Container(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        children: [
                          Text(
                            'Origen',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                          Text(
                            'Destino',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                          Text(
                            'Conductor',
                            style:
                                TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                        ],
                      )),
                  ButtonBar(
                    alignment: MainAxisAlignment.start,
                    children: [
                      FlatButton(
                        textColor: const Color(0xFF6200EE),
                        onPressed: () {
                          // Perform some action
                        },
                        child: const Text('Iniciar'),
                      ),
                      FlatButton(
                        textColor: const Color(0xFF6200EE),
                        onPressed: () {
                          // Perform some action
                        },
                        child: const Text('Finalizar'),
                      ),
                    ],
                  ),
                  Image.asset('assets/card-sample-image.jpg'),
                  Image.asset('assets/card-sample-image-2.jpg'),
                ],
              ),
            ),
          ],
        ));
  }
}
